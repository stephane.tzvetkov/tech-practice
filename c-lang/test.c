#include <stdio.h>

int main() {
    float delta_mcp = 2000.0;
    float hv_ipm_n = 4000.0;
    float hv_old = 1;
    float hv_screen_minus_hv_out = 2000;
    float hv_ipm_n_minus_hv_in = 1000;

    system("false || echo plop");

    float hv_in = hv_ipm_n + (hv_old * hv_ipm_n_minus_hv_in);
    printf("[LOG] subRecordTestProcess -> \"HVin = %f\n", hv_in);

    float hv_out = hv_in - (hv_old * delta_mcp);
    printf("[LOG] subRecordTestProcess -> \"HVout = %f\n", hv_out);
    if(hv_out < 0){
        printf("[WARNING] subRecordTestProcess -> \"HVout = HVin - (HVold * DELTAmcp) < 0\" \n");
        return 0;
    }
    float hv_screen = hv_out - (hv_old * hv_screen_minus_hv_out);
    printf("[LOG] subRecordTestProcess -> \"HVscreen = %f\n", hv_screen);
    if(hv_screen < 0){
        printf("[WARNING] subRecordTestProcess -> \"HVscreen = HVout - (HVold * HVscreenMinusHVin) < 0\" \n");
        return 0;
    }
   return 0;
}
