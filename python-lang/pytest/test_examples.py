# Cf. https://buildmedia.readthedocs.org/media/pdf/pytest/latest/pytest.pdf

import pytest
import os
import sys

# Fixtures
################################################################################

@pytest.fixture #this fixture is run before each test calling it (invoked once per test function)
def input_value():
    """ Example of fixture. """
    inp = 39
    return inp

@pytest.fixture(scope="session") # run once before all tests (invoked once per session)
#@pytest.fixture(scope="module") # run once per module
#@pytest.fixture(scope="class") # run once per test class
#(note that higher-scoped fixtures are instantiated first)
def other_input_value():
    """ Example of "scoped" fixture. """
    inp = 39
    return inp

@pytest.fixture(scope="session", autouse=True) # auto run one per session
def auto_fixture():
    """ Example of fixture that don't need to called in order to run """
    print("plop")

# note that fixtures can also call other fixtures themselves (for better modular designs)
@pytest.fixture
def teardown_input_value():
    """ Example of "teardown" fixture. """
    inp = 11
    yield inp # instead of `return inp`
    # all the code after the `yield` statement serves as teardown code
    # and will execute after the tests (depending on the scope):
    inp = 0

@pytest.fixture
def make_customer_record():
    """ Example of "factury" fixture. """

    def _make_customer_record(name):
        return{"name": name, "orders": []}

    return _make_customer_record
    
def test_customer_records(make_customer_record):
    """ Example of "factory" fixture use in test. """

    customer_1 = make_customer_record("user1")
    customer_2 = make_customer_record("user2")
    customer_3 = make_customer_record("user3")

# this fixture will be called one time per param, each time executing all of it's dependent tests
@pytest.fixture(scope="session", params=["string1", "string2"])
def string_input_value(request): # the fixture get access to each parameter throug the "request" obj
    """ Example of "parametrized" fixture. """

    # ... do something with the request containing the param

    return "string"


# Marks
################################################################################

@pytest.mark.xfail # mark a test expected to fail
def test_divisible_by_3(input_value):
    """ Example of "xfail" mark attribute. """
    assert input_value % 3 == 0

#@pytest.mark.xpass # mark a test that passes despite being expected to fail
#def test_divisible_by_3(input_value):
#    """ Example of "xfail" mark attribute. """
#    assert input_value % 3 == 0

@pytest.mark.skip(reason="no way of currently testing this") # mark a test to be skipped
def test_divisible_by_6(input_value):
    """ Example of skipped test function. """
    assert input_value % 6 == 0

def test_divisible_by_9(input_value):
    """ Example of inner skipped test function. """
    if input_value is 0:
        pytest.skip("skipping this condition")
    assert input_value % 9 == 0

@pytest.mark.skipif(sys.version_info < (3, 6), reason="requires python3.6 or higher")
def test_multiplication_11(num, output):
    """ Example of skipif test function. """
    assert 11*num == output

@pytest.mark.parametrize("num, output", [(1, 11), (2, 22), (3, 35), (4, 44)])
def test_multiplication_13(num, output):
    """ Example of parametrized test function. """
    assert 13*num == output

@pytest.mark.parametrize(
    "test_input, expected",
    [("3+5", 8), ("2+4", 6), pytest.param("6*9", 42, marks=pytest.mark.xfail), ("6+6", 12),]
) # note that test params can be marked also
def test_eval(test_input, expected):
    """ Example of advanced parametrized test function. """
    assert eval(test_input) == expected

# Dependencies and ordering
################################################################################

@pytest.mark.dependency()
def test_long():
   pass

@pytest.mark.dependency(depends=['test_long'])
def test_short():
    pass

# ordering with plugin 'pytest-ordering': `$ python -m pip install pytest-ordering`
@pytest.mark.order2
def test_foo():
    assert True

@pytest.mark.order1
def test_bar():
    assert True

# Misc
################################################################################

def test_create_file(tmp_path):
    """ Example of test using built-in tmp dir with the "temp_path" fixture / function argument """
    content = "content"
    tmp_dir = tmp_path / "sub"
    tmp_dir.mkdir()
    tmp_file = tmp_dir / "hello.txt"
    tmp_file.write_text() == content
    assert p.read_text() == content
    assert len(list(tmp_path.iterdir())) == 1
    assert 0

def test_approx():
    """ Example of test using approx """
    0.1 + 0.2 == approx(0.3)
    (0.1 + 0.2, 0.2 + 0.4) == approx((0.3, 0.6))
    {'a': 0.1 + 0.2, 'b': 0.2 + 0.4} == approx({'a': 0.3, 'b': 0.6})
    1.0001 == approx(1, rel=1e-3)

def test_custom_failure_msg():
    """ Example of a failing test producing a custom test failure message. """
    pytest.fail("Custom failure message")

def test_printing_msg():
    """ Example of a test being developed that needs to print something. """
    print("Plop!")
    assert False # fail the test to see what was printed

def print_msg_to_stdout():
    """ Just print a msg to stdout """
    print("message to check")

def test_stdout(capfd):
    """ """
    print_msg_to_stdout()
    out, err = capfd.readouterr()
    assert out == ("message to check\n")

def addone(nb):
    """ Example. """
    return nb + 1

def test_addone():
    """ Test example. """
    assert addone(1) == 2

def sysexit():
    """ Raising error example """
    raise SystemExit(1)

def test_sysexit():
    """ Test raising error example. """
    with pytest.raises(SystemExit):
        sysexit()
