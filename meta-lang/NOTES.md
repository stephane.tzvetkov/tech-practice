- first-class object : ?

<anki>
	<deck dev-lang>
	<tag meta-lang>

	<field front>
	Syntactic sugar
	<field front/>

	<field back>
	Syntactic sugar is syntax within a programming language that is designed to make things easier
	to read or to express. It makes the language "sweeter" for human use: things can be expressed
	more clearly, more concisely, or in an alternative style that some may prefer.
	<field back/>

	<field source>
	https://en.wikipedia.org/wiki/Syntactic_sugar
	<field source/>
<anki/>
