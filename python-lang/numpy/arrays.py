"""
* https://numpy.org/devdocs/user/absolute_beginners.html
* https://numpy.org/devdocs/user/basics.html
* https://numpy.org/devdocs/user/tutorials_index.html
* https://numpy.org/devdocs/user/howtos_index.html
"""
import numpy as np


# basic arrays:
###############

np.array([1, 2, 3])  # [1, 2, 3] : basic array
np.zeros(2)  # [0., 0.] : zeros array
np.ones(2)  # [1., 1.] : ones array
np.empty(2)  # [3.14, 42.] : random array /!\ don't forget to fill every element after creation
np.linspace(0, 10, num=5)  # [0., 2.5, 5., 7.5, 10.] : linear array with start, stop and step
np.arange(4)  # [0., 1., 2., 3.] : range array
np.arange(2, 9, 2)  # [2., 4., 6., 8.] : range array with start, stop and step
np.arange(2, dtype=np.int64)  # [0, 1, 2] : range array with int64 type (instead of default float)


# sorting and concatenating arrays:
###################################

arr = np.array([2, 1, 5, 3, 7, 4, 6, 8])
np.sort(arr)  # [1, 2, 3, 4, 5, 6, 7, 8] : sort an array
# see also argsort, lexsort, searchshorted and partition for other sorting methods

a = np.array([1, 2, 3, 4])
b = np.array([5, 6, 7, 8])
np.concatenate(a, b)  # [1, 2, 3, 4, 5, 6, 7, 8] : concatenate arrays

x = np.array([[1, 2], [3, 4]])
y = np.array([[5, 6]])
np.concatenate((x, y), axis=0)  # [[1, 2], [3, 4], [5, 6]] : concatenate arrays according to axis 0


# shape and size of arrays:
###########################

array_example = np.array([[[0, 1, 2, 3],
                           [4, 5, 6, 7]],
                          [[0, 1, 2, 3],
                           [4, 5, 6, 7]],
                          [[0, 1, 2, 3],
                           [4, 5, 6, 7]]])

array_example.ndim  # 3 : the number of dimensions of an array
array_example.size  # 24 : the total number of elements in an array
array_example.shape  # (3, 2, 4) : the shape of an array


# reshaping an array:
#####################

a = np.array[0, 1, 2, 3, 4, 5]

b = a.reshape(3, newshape=2)  # reshape an array
# [[0, 1]
#  [2, 3]
#  [4, 5]]
#
# newshape is the new shape you want. You can specify an integer or a tuple of integers (the shape
# should be compatible with the original shape).


# add a new axis to an array:
#############################

# with newaxis:
a = np.array([1, 2, 3, 4, 5, 6])  # a.shape is (6,)
row_vector = a[np.newaxis, :]  # row_vector.shape is (1, 6)
col_vector = a[:, np.newaxis]  # col_vector.shpae is (6, 1)

# with expand_dims:
a = np.array([1, 2, 3, 4, 5, 6])  # a.shape is (6,)
row_vector = np.expand_dims(a, axis=0)  # row_vector.shape is (1, 6)
col_vector = np.expand_dims(a, axis=1)  # col_vector.shpae is (6, 1)


# indexing and slicing arrays:
##############################

data = np.array([1, 2, 3])
data[1]  # 2
data[0:2]  # [1, 2]
data[1:]  # [2, 3]

a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
b = a[a < 5]  # [1, 2, 3, 4]
c = a[a >= 5]  # [5, 6, 7, 8, 9, 10, 11, 12]
d = a[a % 2 == 0]  # [2, 4, 6, 8, 10, 12]
e = a[(a > 2) & (a < 11)]  # [3, 4, 5, 6, 7, 8, 9, 10]
five_up = (a > 5) | (a == 5)
# [[False False False False]
#  [ True  True  True  True]
#  [ True  True  True True]]


# stacking (vertically and horizontally) and spliting (vertically and horizontally) arrays:
###########################################################################################

a1 = np.array([[1, 1],
               [2, 2]])
a2 = np.array([[3, 3],
               [4, 4]])
np.vstack((a1, a2))  # stacking vertically
# [[1, 1],
#  [2, 2],
#  [3, 3],
#  [4, 4]]
np.hstack((a1, a2))  # stacking horizontally
# [[1, 1, 3, 3],
#  [2, 2, 4, 4]]

x = np.arange(1, 25).reshape(2, 12)
# [[ 1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12],
#  [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]]
np.hsplit(x, 3)  # horizontal split
# [[1,  2,  3,  4],
#  [13, 14, 15, 16]],
# [[ 5,  6,  7,  8],
#  [17, 18, 19, 20]],
# [[ 9, 10, 11, 12],
#  [21, 22, 23, 24]]
np.hsplit(x, (3, 4))  # horizontal split after the third and fourth column
# [[1, 2, 3],
#  [13, 14, 15]],
# [[ 4],
#  [16]],
# [[ 5, 6, 7, 8, 9, 10, 11, 12],
#  [17, 18, 19, 20, 21, 22, 23, 24]]


# shallow copy (view) and deep copy:
####################################

# Shallow copy (view), by default all NumPy functions and operators use view(s) whenever possible:
a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
b1 = a[0, :]  # [1, 2, 3, 4]
b1[0] = 99
# b1 is now: [99,  2,  3,  4]
# a is now:
# [[99,  2,  3,  4],
#  [ 5,  6,  7,  8],
#  [ 9, 10, 11, 12]]

# Deep copy
b2 = a.copy()


# Basic array operations:
#########################

data = np.array([1, 2])
ones = np.ones(2, dtype=int)

add = data + ones  # [2, 3]: add arrays

substract = data - ones  # [0, 1]: substract arrays

multiply = data * data  # [1, 4]: multiply arrays

divide = data / data  # [1., 1.]: divide arrays

a = np.array([1, 2, 3, 4])
a.sum()  # 10: sum the content of an array

a.max()  # 4: the higher number of an array
a.min()  # 1: the lower number of an array

a = np.array([[0.45053314, 0.17296777, 0.34376245, 0.5510652],
              [0.54627315, 0.05093587, 0.40067661, 0.55645993],
              [0.12697628, 0.82485143, 0.26590556, 0.56917101]])
a.min(axis=0)  # [0.12697628, 0.05093587, 0.26590556, 0.5510652 ]: lower nums of axis 0 of an array

b = np.array([[1, 1], [2, 2]])
row_sum = b.sum(axis=0)  # [3, 3]: sum the rows of an array
col_sum = b.sum(axis=1)  # [2, 4]: sum the columns of an array

data = np.array([1.0, 2.0])
to_km = data * 1.6  # [1.6, 3.2]: apply a "multiply" broadcast operation


# Manipulating matrices:
########################
# https://numpy.org/devdocs/user/absolute_beginners.html#creating-matrices
