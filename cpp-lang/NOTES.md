- argument / parameter:
	- argument: value/variable/reference being passed in to a function
	- parameter: variable received within the function

> [Anki card] What's an argument ? :: It's a value/variable/reference being passed in to a function.
> [Anki card] What's a parameter ? :: It's a variable received within the function.

---

- arithmetic types are divided into two categories:
	- integral types (characters, integers, boolean)
	- floating-point types (floating points)

- assignement: (!= initialization) Obliterates an object's current value and replaces that value by
  a new one.

- behavior
	- undefined behavior: The program might appear to work, it might crash, or it might produce
	  garbage values.
	- implementation-defined behavior: The program is assuming that the size of e.g. an arithmetic
	  type is fixed and known, whereas it's nonportable because it depends on the machine.

- block: Sequence of zero or more statements enclosed in curly braces.

- buffer: A region of storage used to hold data.

- class: Facility for defining data structures with associated operations.
	- built-in classe: Type built into the language / defined by the language.
	- class type: Type defined by a class (programmer).
	- library type: Type defined by the standard library.

- expression & statement:
	- expression: The smallest unit of computation, consisting of one or more operands and usually one
      or more operators.
	- statement: A part of a program that specifies an action to take place when the program is
	  executed. An expression followed by a semicolon is a statement; other kinds of statements
	  include blocks and control flow statements ("if", "for", "while"...), all of which contain
	  other statements within themselves.

- function: A named unit of computation, composed by:
	- function name: Name by which a function is known and can be called.
	- parameter list: Possibly empty list that specifies what arguments can be used to call the function.
	- function body: Block that defines the actions performed by a function.
	- 
  a function can be described as a:
  	- member function: Unit of computation defined by a class that operate on a specific object.
	- method: Synonym for member function.
	-

- header: Mechanism whereby the definitions of a class or other names are made available to multiple
  programs. A program uses a header through a "#include" directive.

- initialization: (!= assignement) Give an object a value at the moment it is created.

- literal: "self-evident" value that is assigned to a type, e.g. in this instruction "int i = 42;",
  42 is a literal of type int.
	- literal prefix (e.g. "u", "L" or "u8" for a char) to specify a type in a literal
	- literal suffix (e.g. "u", "l" or "ll" for an int) to specify a type in a literal

- manipulator: Object (such as "std::endl"), that when read or written "manipulates" the stream
  itself

- namespace: Mechanism for putting names defined by a library into a single place. Namespaces help
  avoid inadvertent name clashes. E.g. the C++ standard library namespace is "std".

- signed / unsigned: A signed type represents negative or positive numbers (including zero), an
  unsigned type represents only values greater than or equal to zero.

- variable: A named storage that a program can manipulate. Each variable has a type,
  determining the size and layout of the variable's memory, and the set of operations that can be
  applied to the variable.
	- varible definition: It consists of a type specifier, followec by a list of one or more
	  variable names separated by commas, ending with a semicolon. Each name in the list has the
	  type defined by the type specifier. A definition may (optionally provide an initial value for
	  one or more of the names it defines.

- object: An object is a region of memory that can contain data and has a type.
  Some use the term "object" only to refer to variables or values of class types. Others distinguish
  between named and unnamed objects, using the term "variable" to refer to named objects. Still
  others distinguish between objects and values, using the term "object" for data that can be
  changed by the progam and the term "value" for data that are read-only.
