/* Console Hyper Link */
public class CHL {

    private CHL() {
    }

    public static void print(String s) {
        // An index of 1 references the calling method:
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        System.out.println("(" + ste.getFileName() + ":" + ste.getLineNumber() + ") " + s);
    }

    public static void stack(int level) {
        // An index of 1 references the calling method:
        final StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        final int init = Math.min(stackTrace.length - 2, level - 1);
        for (int i = init; i >= 0; i--) {
            StackTraceElement ste = stackTrace[1 + i];
            System.out.println("(" + ste.getFileName() + ":" + ste.getLineNumber() + ") ");
        }
    }

    public static String get() {
        // An index of 1 references the calling method:
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        return "(" + ste.getFileName() + ":" + ste.getLineNumber() + ")";
    }

    public static String getWithoutHyperLink() {
        // An index of 1 references the calling method:
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        return ste.getFileName() + ":" + ste.getLineNumber();
    }
}