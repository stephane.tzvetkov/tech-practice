"""
Multi-threading example
"""

import threading
import time


def thread_example(thread_name: str, delay: int, count_limit: int = -1):

    if delay < 0:
        raise Exception("The delay cannot be less than 0!")

    if count_limit == -1:
        while True:
            time.sleep(delay)
            # add your own commands here
            print(f"{thread_name}: {time.ctime(time.time())}")
    elif count_limit == 0:
        # add your own commands here
        print(f"{thread_name}: {time.ctime(time.time())} (oneshot, 1/1)")
    elif count_limit > 0:
        count: int = 0
        while count < count_limit:
            time.sleep(delay)
            count += 1
            # add your own commands here
            print(f"{thread_name}: {time.ctime(time.time())} ({count}/{count_limit})")
    else:
        raise Exception("The count_limit cannot be less than -1!")


try:

    t1 = threading.Thread(
        target=thread_example,
        args=("thread-1", 0, 0),
    )
    t2 = threading.Thread(
        target=thread_example,
        args=("thread-2", 2, 10),
    )
    t3 = threading.Thread(
        target=thread_example,
        args=("thread-3", 1, -1),
    )

    t1.start()
    t2.start()
    t3.start()

    # wait until thread 1 is completely executed:
    t1.join()
    # wait until thread 2 is completely executed:
    t2.join()
    # wait until thread 3 is completely executed:
    t3.join()  # Note that this thread will never end if its count_limit is set to -1

    # both threads completely executed
    print("Done!")

except Exception as exc:
    print("Error: unable to start a thread!")
    raise exc
