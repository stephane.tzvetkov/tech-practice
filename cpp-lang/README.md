# References

## C++ references

- http://www.stroustrup.com/
- http://www.stroustrup.com/bs_faq.html
- http://www.stroustrup.com/bs_faq2.html
- https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md
- https://en.cppreference.com/w/

## CMake references

- https://cliutils.gitlab.io/modern-cmake
- https://cmake.org/documentation

# TODO

## Debug

-

## Unit tests

- https://stackoverflow.com/questions/242926/comparison-of-c-unit-test-frameworks
- https://github.com/boost-experimental/ut#benchmarks

