#!/bin/sh

# Cf. https://buildmedia.readthedocs.org/media/pdf/pytest/latest/pytest.pdf

# Install pytest:
python -m pip install pytest
pytest --version

# Run pytest:
pytest
pytest -v # (--verbose) increase verbosity: recommanded
pytest -q # (--quiet) decrease verbosity

# Run pytest on specific dir:
pytest ./tests

# Run pytest on specific file:
pytest ./test_specific_file.py

# Run pytest on specific test:
pytest ./test_specific_file.py::test_func
pytest ./test_specific_file.py::TestClass::test_func

# Show builtin and custom fixtures / function arguments:
pytest -v --fixtures

# Run pytest and stop after first failure:
pytest -x

# Run pytest and stop after two failures:
pytest --maxfail=2

# Run pytest by keyword expressions:
pytest -k "slow long" # run tests which contain names match the given expression (case insensitive)

# Run tests by marker expressions:
pytest -m "fast" # run tests which are decorated with @pytest.mark.fast

# Create JUnitXML format result files (which can be read e.g. by Jenkins or other CI servers):
pytest --junitxml=/path/to/resulting/junitxml

# Re-run last failed tests:
pytest --last-failed # (--lf) only re-run last failures

# Find the 3 slowest tests:
pytest --durations=3

# pytest plugins to check:
################################################################################
# cf. complete list here https://plugincompat.herokuapp.com/
# and here https://pypi.org/search/?q=pytest-

python -m pip install pytest-cov # to report tests coverage
python -m pip install pytest-xdist # to distribute tests to CPUs and remote hosts...
python -m pip install pytest-instafail # to report failures while the test is running
python -m pip install pytest-falkes # to check source code with pyflakes

