#include <iostream>

int main() {

	// the "while" statement:
	int whileSum = 0, val = 1;
	while(val <= 10) {
		whileSum += val;
		++val;
	}
	std::cout << "The \"while\" sum of 1 to 10 inclusive is " << whileSum << std::endl;

	// the "for" statement:
	int forSum = 0;
	for(int val = 1; val <= 10; ++val) {
		forSum += val;
	}
	std::cout << "The \"for\" sum of 1 to 10 inclusive is " << forSum << std::endl;

	// the "if" statement:
	if(whileSum == 55 and forSum == 55){ // same as "if(whileSum == 55 && forSum == 55) {"
		std::cout << "The \"for\" sum and the \"while\" sum of 1 to 10 inclusive are equals!"
			<< std::endl;
	} else if (whileSum != 55 or forSum != 55) { // same as "if(whileSum != 55 || forSum != 55) {"
		std::cout << "At least one of those sum wasn't the sum of 1 to 10 inclusive!" << std::endl;
	} else {
		std::cout << "You shouldn't reach to code block!" << std::endl;
	}

	return 0;
}

