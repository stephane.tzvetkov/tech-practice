
What is an array?
- a grid of elements that can be indexed in various ways, and containing information about how to
  locate an element and how to interpret it.

What's the main difference between a Python list and a NumPy array?
- Python list can contain different data types within a single list, all of the elements in a NumPy
  arry should be homogeneous.

What is the type of the elements of a Numpy array?
- The all have the same type: "dtype" (data type).

How can be indexed an array (4 responses)?
- by a tuple of nonnegative integers
- by booleans
- by another array
- by integers

What's the rank of an array?
- the number of dimensions of the array.

What's the shape of an array?
- a tuple of integers giving the size of the array along each dimensions.

What's an "ndarray"?
- an n-dimensional array.

What's a matrix (in terms of arrays)?
- a two dimensional array.

What's a tensor (in terms of arrays)?
- a three or higher dimensional array.

In NumPy, what's another name for dimensions?
- axes. E.g. this array: [[0., 0., 0.],[1., 1., 1.]] has two axes, the first axis has a length of 2
  ("columns" length), the second axis has a lenght of 3 ("row" length).

What's the fastest NumPy array to create?
- empty array (but it's content is random, depending on the state of the memory: don't forget to
  fill every elements after creating it)

What's a NumPy view?
- a shallow copy of an array. /!\ modifying data in a view also modifies the
  original array.

What will return NumPy functions and operators (like indexing and slicing) whenever possible?
- view(s).

How to get a deep copy of a NumPy array?
- with the "copy" method
