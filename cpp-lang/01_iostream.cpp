#include <iostream>

/*
 * simple main function illustrating the use of iostream:
 */
int
main()
{
    std::cout << "Enter two numbers:" << std::endl;

    int v1 = 0, v2 = 0;

    // reads two inputs, one after the other:
    std::cin >> v1 >> v2;

    // prints variables:
    std::cout << "The sum of " << v1 << " and " << v2 << " is " << v1 + v2 << std::endl;

    std::clog << "Calcul is over" << std::endl;

    std::cerr << "Error message";
    std::cerr << "example" << std::endl;

    std::cout << "________________" << std::endl;

    std::cout << "Enter numbers, as many as you want (enter \"q\" to stop):" << std::endl;
    int sum = 0, value = 0;
    while (std::cin >> value) {
        sum += value;
    }
    std::cout << "The total sum is: " << sum << std::endl;

    return 0;
}
