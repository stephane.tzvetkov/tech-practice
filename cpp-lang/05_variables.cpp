#include <ostream>
#include <string>
#include <iostream>

int main() {

	// initializes multiples integer at the time:
	int sum = 0, value, units_nb = 42;

	// initializes strings iin different ways:
	std::string str1 = "plop";
	std::string str2 = {"plop"}; // list initialization
	std::string str3{"test"}; // list initialization
	std::string str4("test");

	std::cout << str3 << std::endl;

	// initializes ints in different ways:
	int nb1 = 0; // copy initialization
	int nb2 = {0}; // list initialization
	int nb3{0}; // list initialization
	int nb4(0); // direct initialization
	int nb5 = nb4;

    std::string s1{}; // Value initialization
    std::string s2("hello"); // Direct initialization
    std::string s3 = "hello"; // Copy initialization
    std::string s4{'a', 'b', 'c'}; // List initialization
    char a[3] = {'a', 'b'}; // Aggregate initialization
    char& c = a[0]; // Reference initialization

	return 0;
}
