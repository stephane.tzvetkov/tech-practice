#include <iostream>

// this header doesn't exist, just imagine it's there and see how to use the classe it defines:
#include "class_item.h"

int main() {
	class_item ci1, ci2;
	std::cin >> ci1;
	std::cout << ci1 << std::endl;

	std::cin >> ci1 >> ci2;

	if(ci1.id() == ci2.id()) {
		std::cout << ci1 + ci2 << std::endl;
		return 0;
	} else {
		std::cerr << "Data must refer to same ISBN" << std::endl;
		return -1;
	}
}

