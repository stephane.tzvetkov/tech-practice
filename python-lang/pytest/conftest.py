# Cf. https://buildmedia.readthedocs.org/media/pdf/pytest/latest/pytest.pdf

import pytest

@pytest.fixture # run this fixture before the test calling it
def global_input_value():
    """ Example of global fixture available from anywhere in the current dir. """
    inp = 42
    return inp

#def pytest_report_header(config):
#    """ Example of pytest results header addition on one line. """
#    return "additional info to test header results"

# Cf. https://stackoverflow.com/a/35394239

def pytest_report_header(config):
    """ Example of pytest results header addition on multiple lines, only if verbose. """
    if config.getoption("verbose") > 0:
        return ["additional info to test header results...", "...with a second line", "..."]

def pytest_configure(config):
    """
    Allows plugins and conftest files to perform initial configuration.
    This hook is called for every plugin and initial conftest
    file after command line options have been parsed.
    """

def pytest_sessionstart(session):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """

def pytest_sessionfinish(session, exitstatus):
    """
    Called after whole test run finished, right before
    returning the exit status to the system.
    """

def pytest_unconfigure(config):
    """
    called before test process is exited.
    """
